function notEmptyArray(arrayCandidate) {
    return arrayCandidate && arrayCandidate.length && arrayCandidate[0];
}

function orderBookNotEmpty(orderbook) {
    return notEmptyArray(orderbook.buy) || notEmptyArray(orderbook.sell);
}

function roundValue(value, decimalPlaces, floor) {
  let precision = Math.pow(10, decimalPlaces);
  let integerValue = Math.floor(value);
  let decimalValue = value - integerValue;

  let roundedDecimalValue = 0;

  if (floor)
    roundedDecimalValue = Math.floor(decimalValue * precision) / precision;
  else
    roundedDecimalValue = Math.round(decimalValue * precision) / precision;

  let result = integerValue + roundedDecimalValue;

  return result;
}

function roundCryptoValue(value, floor) {
  return roundValue(value, 8, floor);
}

function getBaseCurrencyFromMarketName(marketName) {
  return marketName.split('-')[0];
}

module.exports = {
  roundValue,
  roundCryptoValue,
  notEmptyArray,
  orderBookNotEmpty,
  getBaseCurrencyFromMarketName
};