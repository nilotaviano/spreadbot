// fake ecosystem file to run as a pm2 process and test pm2 commands
module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [

        {
            name: 'fake-spreadbot',
            script: './app.js',
            watch: false,
            env: {
                APIKEY: 'cd3e344c80464574a57e899248904',
                APISECRET: '7907f3001b994014ab5f6e66a5f7d',
                ACTIVE_BOTS: 2,
                BASE_MARKET: 'FAKEMARKET',
                CONFIG: {
                    'BTC': {
                        minimumSpread: 1.032, // The quocient of ask/bid
                        minimumVolume: 100, // The minimum volume to be considered in targetBaseCurrency of the market
                        minimumAmountToInvest: 0.02, // The minimum amount of targetBaseCurrency to invest
                        maximumNegotiatedAmount: 0.04,
                        minimumPrice: 0.00001000
                    },
                    'ETH': {
                        minimumSpread: 1.05, // The quocient of ask/bid
                        minimumVolume: 25, // The minimum volume to be considered in targetBaseCurrency of the market
                        minimumAmountToInvest: 0.04, // The minimum amount of targetBaseCurrency to invest
                        maximumNegotiatedAmount: 0.04,
                        minimumPrice: 0.00020000
                    }
                }
            }
        },
    ],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy: {
        production: {
            user: 'node',
            host: '212.83.163.1',
            ref: 'origin/master',
            repo: 'git@github.com:repo.git',
            path: '/var/www/production',
            'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
        },
        dev: {
            user: 'node',
            host: '212.83.163.1',
            ref: 'origin/master',
            repo: 'git@github.com:repo.git',
            path: '/var/www/development',
            'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env dev',
            env: {
                NODE_ENV: 'dev'
            }
        }
    }
};
