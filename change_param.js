#!/usr/bin/env node

/**
    Example usage: node change_param.js ETH minimumSpread 1.05
    - Will change the spread on the ETH configuration to 1.05
**/

var pm2 = require('pm2');
var parseArgs = require("./command_line/command_line_parser");

var topic = 'change_params';

var argsMapper = {
    "-p": { name: "processName", mandatory: true },
    "-an": { name: "argumentName", mandatory: true },
    "-av": { name: "argumentValue", mandatory: true },
    "-bm": { name: "baseMarket" }
};

if (process.argv && process.argv.length > 1 && process.argv[1] == __filename) { // if this file was executed directly
    var args = process.argv.slice(2);
    var options = parseArgs(argsMapper, args, "change_param -p spreadbot-btc -bm BTC -an minimumSpread -av 1.02");
    run(options, true);
}

function run(options, standaloneProcess) {

    if (!options) {
        if (standaloneProcess)
            process.exit(-1);
        else
            return;
    }
    
    pm2.connect(function(err) {

        if (err) throw new Error('err');

        console.log(options);

        var processName = options.processName;

        pm2.launchBus(function(err, bus) {
            if (err) throw new Error('err');

            bus.on('process:msg', function(packet) {
                if(packet.data.topic != topic) // If it's not the response we expect, return
                    return;
                    
                if(packet.data.success)
                    console.log("Parameters changed for bot " + packet.process.name);
                else
                    console.error("Error trying to change params from " + packet.process.name);

                if (standaloneProcess) {
                    // Disconnect to PM2
                    pm2.disconnect(function() {
                        process.exit(0);
                    });
                }
            });
        });

        // Get all processes running
        pm2.list(function(err, process_list) {
            if (!err)
                for (var i in process_list) { // seek a better way to find a process by its name
                    if (process_list.hasOwnProperty(i)) {
                        var process_i = process_list[i];

                        if (process_i.name != processName)
                            continue;

                        pm2.sendDataToProcessId(process_i.pm2_env.pm_id, {
                            topic: topic,
                            type: 'change',
                            data: options,
                            id: process_i.pm2_env.pm_id
                        });
                        console.log('worker_id', process_i.pm2_env.pm_id);
                    }
                }
        });
    });
}
