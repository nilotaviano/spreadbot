function parseArgs(argsMapper, args, usageSample) {
  var options = {};

  var errorOccurred = false;

  if (args && args.length > 0) {

    if (args[0] == "help") {
      console.info(argsMapper);
      console.info("\nEx.: " + usageSample);
      return;
    }

    for (var i = 0; i < args.length && !errorOccurred; i++) {
      var currentArg = args[i];

      if (!argsMapper.hasOwnProperty(currentArg)) {
        errorOccurred = true;
        console.error("Unkown argument " + currentArg);
        console.error("Valid options are:");
        console.error(argsMapper);
      }
      else {
        var argValue = undefined;
        var argsConfig = argsMapper[currentArg];

        if (argsConfig.isBoolean)
          argValue = true;
        else if (argsConfig.isEnum)
          argValue = currentArg;
        else {
          i = i + 1; // Jump for the argument value
          argValue = args[i];
        }

        var propertyName = argsConfig.name;
        options[propertyName] = argValue;
      }
    }
  }

  if (!errorOccurred) {
    var anyMissingArgument = false;
    for (var argAcronym in argsMapper) {
      var arg = argsMapper[argAcronym];
      if (arg.mandatory && !options.hasOwnProperty(arg.name)) {
        console.error("Mandatory argument not found: " + argAcronym + " (" + arg.name + ")");
        anyMissingArgument = true;
      }
    }
    if (anyMissingArgument) {
      console.error("Type 'help ' for a complete list of arguments");
      errorOccurred = true;
    }
  }

  if (errorOccurred)
    options = null;

  return options;
}

module.exports = parseArgs;