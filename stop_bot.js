#!/usr/bin / env node
'use strict';

const pm2 = require('pm2');
const parseArgs = require("./command_line/command_line_parser");
const path = require("path");
const logger = require('./logger');
const log = logger.log;
const error = logger.error;

var topic = 'stop';

var argsMapper = {
    "stop": { name: "command", isEnum: true },
    "restart": { name: "command", isEnum: true },
    "-p": { name: "processName" }
};

if (process.argv && process.argv.length > 1 && process.argv[1] == __filename) { // if this file was executed directly
    let args = process.argv.slice(2);
    let options = parseArgs(argsMapper, args, "stop_bot -p spreadbot-btc");
    run(options, true);
}

function run(options, standaloneProcess) {

    if (!options) {
        if (standaloneProcess)
            process.exit(-1);
        else
            return;
    } else if (!options.command)
        options.command = "stop";

    let processName = options.processName;

    // Connect or launch PM2
    pm2.connect(function(err) {

        if (err) throw new Error(err);

        let stoppedBotsIds = [];

        pm2.launchBus(function(err, bus) {
            if (err) throw new Error('err');

            bus.on('process:msg', async function(packet) {
                if (packet.data.topic != topic) // If it's not the response we expect, return
                    return;

                let stoppedBotIndex = stoppedBotsIds.indexOf(packet.process.pm_id);

                if (stoppedBotIndex != -1) {
                    if (packet.data.success) {
                        log("Bot " + packet.process.name + " stopped.", true);

                        if (options.command == "restart")
                            await restart(pm2, packet.process.name);
                        else if (options.command == "stop")
                            await stop(pm2, packet.process.name);
                    }
                    else
                        log("Error stopping bot " + packet.process.name, true);

                    stoppedBotIndex = stoppedBotsIds.indexOf(packet.process.pm_id); // Concurrently another "thread" may change the index
                    stoppedBotsIds.splice(stoppedBotIndex, 1);

                    if (stoppedBotsIds.length == 0)
                        finishWork(standaloneProcess);
                }
            });
        });

        // Get all processes running
        pm2.list(function(err, process_list) {
            if (err) throw new Error(err);

            for (let i in process_list) {
                if (process_list.hasOwnProperty(i)) {
                    let process_i = process_list[i];

                    if (process_i.pm2_env.status != "online" || process_i.pm2_env.axm_options.isModule || (processName && process_i.name != processName))
                        continue;

                    pm2.sendDataToProcessId(process_i.pm2_env.pm_id, {
                        type: 'type',
                        data: 'data',
                        id: process_i.pm2_env.pm_id, // the pm_id of the worker you send to
                        topic: topic
                    });

                    stoppedBotsIds.push(process_i.pm2_env.pm_id);

                    log('worker_id', process_i.pm2_env.pm_id);
                }
            }

            if (stoppedBotsIds.length == 0)
                finishWork(standaloneProcess);
        });
    });
}

function finishWork(standaloneProcess) {
    if (standaloneProcess) {
        // Disconnect to PM2
        pm2.disconnect(function () {
            process.exit(0);
        });
    }
}

function stop(pm2, processName) {
    return new Promise((resolve, reject) => {
        pm2.stop(processName, function (err, proc) {
            if (err) {
                log('Error trying to stop process ' + processName, true);
                log(err, true);
                reject();
            }
            else {
                log('Process stopped: ' + processName, true);
                resolve(proc);
            }
        });
    });
}

function restart(pm2, processName) {
    return new Promise((resolve, reject) => {
        pm2.describe(processName, async function(err, processDescription) {
            if (err) {
                log('Error trying to describe process ' + processName, true);
                log(err, true);
                reject();
            }
            else {
                if ("length" in processDescription && processDescription.length == 1)
                    processDescription = processDescription[0];
                let configFilePath = path.format({
                    dir: processDescription.pm2_env.pm_cwd,
                    name: processName,
                    ext: ".config.js"
                });
                await del(pm2, processName);
                let newProcess = await start(pm2, configFilePath);
                log('Process restarted: ' + newProcess.name, true);
                resolve (newProcess);
            }
        });
    });
}

function del(pm2, processName) {
    return new Promise((resolve, reject) => {
        pm2.delete(processName, function(err, proc) {
            if (err) {
                log('Error trying to delete process ' + processName, true);
                log(err, true);
                reject();
            }
            else
                resolve(proc);
        });
    });
}

function start(pm2, jsonConfigPath) {
    return new Promise((resolve, reject) => {
        pm2.start(jsonConfigPath, function(err, proc) {
            if (err) {
                log('Error trying to start process ' + jsonConfigPath, true);
                log(err, true);
                reject();
            }
            else {
                if ("length" in proc && proc.length == 1)
                    proc = proc[0];
                resolve(proc.pm2_env);
            }
        });
    });
}