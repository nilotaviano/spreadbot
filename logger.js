var os = require('os');

var instanceIdentifier = `${process.env.PROCESS_IDENTIFIER || os.hostname()} (${process.env.BASE_MARKET})`;
var webhook = null;

if (process.env.WEBHOOK_URL) {
    var IncomingWebhook = require('@slack/client').IncomingWebhook;

    var webhookURL = process.env.WEBHOOK_URL || '';
    webhook = new IncomingWebhook(webhookURL);
}

function log(message, sendToSlack) {
    console.log(message);

    if (sendToSlack)
        sendMessageToSlack(message);
}

function error(message, sendToSlack) {
  console.error(message);

  if (sendToSlack)
    sendMessageToSlack(message);
}

function sendMessageToSlack(message) {
    if (webhook)
        webhook.send(`${instanceIdentifier}: ${message}`, function(err, res) {
          if (err) {
            console.error('Error:', err);
            }
        });
}

module.exports = { log, error };