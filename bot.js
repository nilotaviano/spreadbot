var logger = require('./logger');
const log = logger.log;
const error = logger.error;
var helper = require("./helper");

var minutesToWaitBeforeAssumingLoss = process.env.MINUTES_FOR_LOSS || 10;
var stepbackwardsPercentage = 0.01;

function Bot(botId, marketName, balance, minimumSpread, callbackFunction, timeoutInMS, exchange, accumulate, startingBidPrice, minTradeSize) {
  this.id = botId;
  this.balance = balance;
  this.initialBalance = balance;
  this.minimumSpread = minimumSpread;
  this.minimumTradeSize = minTradeSize;
  
  this.myCurrentAsk = null;
  this.myCurrentBid = null;
  this.currentOrderUID = null;

  this.purchasedAmount = 0;
  this.purchasedPrice = null;
  this.purchasedTime = null;
  this.targetMarket = marketName;
  this.baseMarket = helper.getBaseCurrencyFromMarketName(this.targetMarket);
  this.callbackFunction = callbackFunction;

  this.timeoutInMS = timeoutInMS;

  this.exchange = exchange;

  this.stopMessageReceived = false;

  this.accumulate = accumulate;
  this.accumulatedEquivalentInBaseCurrency = 0; // To account for profits

  this.startingBidPrice = startingBidPrice;

  this.reportCallback = null;
}

Bot.prototype = {
  constructor: Bot
};

Bot.prototype.start = function () {
  this.makeMoney();
};

Bot.prototype.makeMoney = function () {
  this.log('makeMoney');

  if (this.reportCallback)
    this.reportCallback(this);

  // Only 1 order can be active at any time
  if (this.hasBoughtEnough()) {
    this.tryToSell();
  }
  else if (this.hasEnoughBalanceToKeepBuying()) {
    this.tryToBuy();
  }
  else
    this.finishWork();
};

Bot.prototype.hasBoughtEnough = function () {
  if (!(this.purchasedAmount > 0))
    return false;

  let boughAmountInBaseMarket = helper.roundCryptoValue(this.purchasedAmount * this.purchasedPrice, true);
  let exchangeMinimumTrade = this.exchange.getMinimumTrade(this.baseMarket) * 1.1; // 10% so it's not so risky to sell

  return boughAmountInBaseMarket > exchangeMinimumTrade && this.purchasedAmount > this.minimumTradeSize;
};

Bot.prototype.hasEnoughBalanceToKeepBuying = function () {
  return this.balance > this.exchange.getMinimumTrade(this.baseMarket);
};

Bot.prototype.tryToBuy = function () {
  this.log('tryToBuy');
  this.exchange.getorderbook({
    market: this.targetMarket,
    depth: 5
  }, (response) => {
    if (response.success && response.result && helper.notEmptyArray(response.result.buy) && helper.notEmptyArray(response.result.sell)) {
      var bidIndex = 0;

      // Check if there is a single bid trying to up the rate too much
      // If so, we will consider the second highest bid when increasing the price
      if (response.result.buy[bidIndex].Rate > response.result.buy[bidIndex + 1].Rate * (1 + stepbackwardsPercentage))
        bidIndex++;

      var targetBid = parseFloat(response.result.buy[bidIndex].Rate);
      var ask = parseFloat(response.result.sell[0].Rate);

      var spread = ask / targetBid;

      if (this.stopMessageReceived) {
        var message = "Stop message received";
        this.log(message);
        this.finishWork();
      }
      else if (spread < this.minimumSpread) {
        message = "Spread closed for market " + this.targetMarket;
        this.log(message);
        this.finishWork();
      }
      else if (targetBid > this.startingBidPrice * 1.05) { // Price rose 5% while bot was running
        message = `Price rose from ${this.startingBidPrice} to ${targetBid}. Stopping bot.`;
        this.log(message);
        this.finishWork();
      }
      else {
        if (!this.myCurrentBid || targetBid > this.myCurrentBid)
          this.setBid(targetBid + 0.00000001);
        else if (this.myCurrentBid > targetBid * (1 + stepbackwardsPercentage)) // Check if there is only one order holding the price besides this one
          this.cancelExistingOrder(() => this.makeMoney());
        else
          setTimeout(() => this.checkExistingOrder(), this.timeoutInMS);
      }
    }
    else {
      this.error('Error trying to getorderbook: ' + response.message);
      this.finishWork();
    }
  });
};

Bot.prototype.setBid = function (rate) {
  this.log('setBid');
  this.cancelExistingOrder((error) => {
    if (!error && !(this.purchasedAmount > 0)) {
      var balanceToConsider = Math.min(this.balance, this.initialBalance);
      rate = Math.ceil(rate * 100000000) / 100000000; // Fix in 8 decimal places
      var quantity = Math.floor(balanceToConsider / rate * 100000000) / 100000000;

      this.exchange.buylimit({
        market: this.targetMarket,
        quantity: quantity,
        rate: rate
      }, (response) => {
        if (response.success && response.result) {
          this.myCurrentBid = rate;
          this.currentOrderUID = response.result.uuid;

          var message = 'Placed order to buy ' + this.targetMarket + ' at ' + rate + ". Order UID: " + this.currentOrderUID;
          this.log(message);
          setTimeout(() => this.checkExistingOrder(), this.timeoutInMS);
        }
        else {
          message = `Error trying to buylimit: ${response.message}. Market: ${this.targetMarket}. Amount: ${quantity}. Price: ${rate}. Total: ${quantity * rate}`;
          this.error(message);
          this.makeMoney();
        }
      });
    }
    else
      setTimeout(() => this.makeMoney(), this.timeoutInMS);
  });
};

Bot.prototype.tryToSell = function () {
  this.log('tryToSell');
  this.exchange.getorderbook({
    market: this.targetMarket
  }, (response) => {
    if (response.success && response.result && helper.notEmptyArray(response.result.sell)) {
      var askIndex = 0;

      // Check if there is a single ask trying to drop the rate too much
      // If so, we will consider the second lowest ask when decreasing the price
      if (response.result.sell[askIndex].Rate < response.result.sell[askIndex + 1].Rate * (1 - stepbackwardsPercentage)) {
        askIndex = askIndex + 1;
      }

      var targetAsk = parseFloat(response.result.sell[askIndex].Rate);

      if ((!this.myCurrentAsk || targetAsk < this.myCurrentAsk) && this.isNotAssumingUnecessaryDeficit(targetAsk))
        this.setAsk(targetAsk - 0.00000001);
      // If the current ask is mine and the others asks are way higher, cancel
      else if (this.myCurrentAsk && this.myCurrentAsk < targetAsk * (1 - stepbackwardsPercentage)) // Check if there is only one order holding the price besides this one
        this.cancelExistingOrder(() => this.makeMoney());
      else
        setTimeout(() => this.checkExistingOrder(), this.timeoutInMS);
    }
    else {
      this.log('Error trying to getorderbook: ' + response.message);
      setTimeout(() => this.makeMoney(), this.timeoutInMS);
    }
  });
};

Bot.prototype.isNotAssumingUnecessaryDeficit = function (sellPrice) {
  var currentDate = new Date();
  var feeMultiplier = 1 + this.exchange.defaultFee * 2;
  return this.purchasedPrice * feeMultiplier < sellPrice || ((currentDate - this.purchasedTime) / (1000 * 60)) > minutesToWaitBeforeAssumingLoss; // Wait 10 minutes to assume deficit
};

Bot.prototype.setAsk = function (rate) {
  this.log('setAsk');
  this.cancelExistingOrder((error) => {
    if (!error && this.purchasedAmount > 0) {
      rate = Math.ceil(rate * 100000000) / 100000000; // Fix in 8 decimal places

      var amountToSell;

      if (this.accumulate) {
        amountToSell = ((rate * this.purchasedAmount) / (rate / this.purchasedPrice) / rate) * (1 + 2 * this.exchange.defaultFee);
        amountToSell = Math.min(amountToSell, this.purchasedAmount);

        if (amountToSell < this.purchasedAmount)
          this.log(`Accumulating ${this.purchasedAmount - amountToSell} ${this.targetMarket}`)
      }
      else
        amountToSell = this.purchasedAmount;

      this.exchange.selllimit({
        market: this.targetMarket,
        quantity: amountToSell,
        rate: rate
      }, (response) => {
        if (response.success && response.result) {
          this.myCurrentAsk = rate;
          this.currentOrderUID = response.result.uuid;

          var message = 'Placed order to sell ' + this.targetMarket + ' at ' + rate + ". Order UID: " + this.currentOrderUID;
          this.log(message);
          setTimeout(() => this.checkExistingOrder(), this.timeoutInMS);
        }
        else {
          message = `Error trying to selllimit. ${response.message}. Market: ${this.targetMarket}. Amount: ${amountToSell}. Price: ${rate}. Total: ${amountToSell * rate}`;

          if (response.message.startsWith('DUST_TRADE_DISALLOWED_MIN_VALUE') || response.message == 'MIN_TRADE_REQUIREMENT_NOT_MET' || response.message == 'INSUFFICIENT_FUNDS') {
            this.purchasedAmount = 0;
            this.purchasedPrice = null;
          }

          this.error(message);
          this.makeMoney();
        }
      });
    }
    else
      setTimeout(() => this.makeMoney(), this.timeoutInMS);
  });
};

Bot.prototype.checkExistingOrder = function (callback) {
  this.log('checkExistingOrder');
  if (!callback) {
    callback = () => this.makeMoney();
  }

  if (this.currentOrderUID == null) {
    callback();
  }
  else {
    this.exchange.getorder({
      uuid: this.currentOrderUID
    }, (response) => {
      if (response.success && response.result) {
        var message = undefined;
        if (!this.orderIsOpen(response.result)) { // Order cancelled or executed
          this.updateDataFromOrder(response.result);

          if (response.result.OrderUuid == this.currentOrderUID) {
            this.currentOrderUID = null;
            this.myCurrentAsk = this.myCurrentBid = null;

            // If sold everything and is at a loss, stop trading in this market for now
            if (this.purchasedAmount == 0 && this.balance < 0.99 * this.initialBalance) {
              callback = () => this.finishWork();
            }
          }

          if (!response.result.CancelInitiated) // Order executed
            message = 'Order executed:\n' + JSON.stringify(response.result);
          else {
            message = 'Order cancelled: ' + JSON.stringify(response.result);
          }
        }
        else if (response.result.QuantityRemaining != response.result.Quantity) {
          message = 'Order partially executed:\n' + JSON.stringify(response.result);
        }

        if (message)
          this.log(message);

        if (response.result.IsOpen && response.result.CancelInitiated) // If tried to cancel, but it's not cancelled yet, check again
          this.checkExistingOrder(callback);
        else
          setTimeout(() => callback(response.result), this.timeoutInMS);
      }
      else {
        message = `Error trying to getorder #${this.currentOrderUID}: ` + response.message;
        this.error(message);

        setTimeout(() => this.checkExistingOrder(callback), this.timeoutInMS);
      }
    });
  }
};

Bot.prototype.updateDataFromOrder = function (order) {
  this.log("Updating data for order " + order.OrderUuid);

  // Workaround for poloniex canceled orders
  if (order.Quantity == 0 && order.CancelInitiated) {
    this.log("Order was canceled without executed trades");
    return;
  }

  // If the order closed, update balance
  if (!this.orderIsOpen(order) && order.Quantity != order.QuantityRemaining) {
    if (isSell(order))
      this.balance = this.balance + (order.Price - order.CommissionPaid);
    else
      this.balance = this.balance - (order.Price + order.CommissionPaid);
  }

  if (isBuy(order)) {
    this.purchasedAmount = order.Quantity - order.QuantityRemaining;
    this.purchasedPrice = this.purchasedAmount > 0 ? order.PricePerUnit : null;

    if (this.purchasedAmount > 0 && this.purchasedTime == null)
      this.purchasedTime = new Date();
  }
  else if (isSell(order)) {
    this.purchasedAmount = this.purchasedAmount - (order.Quantity - order.QuantityRemaining);

    // If accumulating and sold enough to have the initial balance back
    // The exchange fee is discounted to account for a margin of error
    if (this.accumulate && this.balance >= this.initialBalance * (1 - this.exchange.defaultFee)) {
      this.accumulatedEquivalentInBaseCurrency += this.purchasedAmount * order.PricePerUnit;
      this.purchasedAmount = 0;
    }

    if (this.purchasedAmount <= 0)
      this.purchasedPrice = null;

    if (order.QuantityRemaining == 0) // If sold everything, clean the purchasedTime
      this.purchasedTime = null;
  }
};

Bot.prototype.orderIsOpen = function (order) {
  return order.IsOpen && !this.isSellRemainingBelowMinimum(order); // If the order remaining is below minimum, it should be treated as closed
}

Bot.prototype.isSellRemainingBelowMinimum = function (order) {
  if (!isSell(order))
    return false;

  // If the quantity remaining is below minimumTrade, forget about the order so it will not accumulate dust
  var remaining = order.QuantityRemaining * order.Limit;

  var isBelowMinimum = remaining <= this.exchange.getMinimumTrade(this.baseMarket);

  if (isBelowMinimum)
    order.QuantityRemaining = 0;

  return isBelowMinimum;
};

function isSell(order) {
  return order.Type == 'LIMIT_SELL';
};

function isBuy(order) {
  return order.Type == 'LIMIT_BUY';
};

Bot.prototype.cancelExistingOrder = function (callback) {
  this.log('cancelExistingOrder');
  if (this.currentOrderUID == null)
    callback();
  else {
    this.exchange.cancelorder({
      uuid: this.currentOrderUID
    }, (response) => {
      if (response.success || response.message == 'ORDER_NOT_OPEN') {
        var message = 'Order cancelling initiated: ' + this.currentOrderUID;
        this.log(message);

        var ensureOrderCancelled = () => {
          this.checkExistingOrder((order) => {
            if (!order || !order.IsOpen)
              callback();
            else {
              ensureOrderCancelled();
            }
          });
        };
        ensureOrderCancelled();
      }
      else {
        message = `Error cancelling order #${this.currentOrderUID}: ` + response.message;
        this.error(message);

        setTimeout(() => {
          this.cancelExistingOrder(callback);
        }, this.timeoutInMS);
      }
    });
  }
};

Bot.prototype.log = function (message) {
  message = this.decorateBotMessage(message);

  log(message);
};

Bot.prototype.error = function (message) {
  message = this.decorateBotMessage(message);

  error(message);
};

Bot.prototype.decorateBotMessage = function (message) {
  return "Bot " + this.id + " (" + this.targetMarket + ")> " + message;
}

Bot.prototype.stop = function () {
  this.stopMessageReceived = true;
};

Bot.prototype.finishWork = function () {
  this.log('finishWork');
  this.cancelExistingOrder((error) => {
    if (error)
      this.error('Error trying to finishWork: ' + error);

    // Don't leave without completely selling what was bought
    if (this.hasBoughtEnough())
      this.tryToSell();
    else
      this.callbackFunction(this);
  });
};

// export the class
module.exports = Bot;
