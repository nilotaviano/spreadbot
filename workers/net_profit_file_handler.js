﻿'use strict'

const fs = require('fs');

let netProfitPerDay = {};

let fileName = 'netProfit-'+process.env.PROCESS_IDENTIFIER+'.json'

let initialize = () => {
  fs.readFile(fileName, 'utf8', function (err, data) {
    if (!err) {
      netProfitPerDay = JSON.parse(data);
    }
  });
};

let recordActualProfitFromBot = (bot) => {
  try {
    var actualProfit = (bot.balance + bot.accumulatedEquivalentInBaseCurrency) - bot.initialBalance;

    if (actualProfit != 0) {
      var now = new Date();
      var today = new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate())).toISOString().substring(0, 10); // string in YYYY-MM-DD format

      var netProfit = netProfitPerDay[today] ? netProfitPerDay[today] : 0;

      netProfit += actualProfit;

      netProfitPerDay[today] = netProfit;

      fs.writeFile(fileName, JSON.stringify(netProfitPerDay, null, '\t'), function (err) {
        if (err)
          console.error("Error writing netProfit.json:\n" + JSON.stringify(err));
      });
    }
  }
  catch (ex) {
    console.error("Exception while writing netProfit.json:\n" + JSON.stringify(ex));
  }
};

module.exports = {
  initialize,
  recordActualProfitFromBot
};
