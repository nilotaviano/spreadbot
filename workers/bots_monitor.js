﻿'use strict'

const logger = require('./../logger');
const log = logger.log;
const error = logger.error;
const emoji = require('node-emoji')
const minutesToTriggerAlert = (process.env.MINUTES_FOR_LOSS || 0) + 1;  // The maximum amount of time a bot wouldn't normally report is the time it spends in a loop before selling at a loss

class BotsMonitor {

    constructor() {
        this.botsToMonitor = [];
        this.unresponsiveBots = [];
        this.botsLastReport = {};

        this.timerObj = null;
    }

    monitorBot(bot) {
        this.botsToMonitor.push(bot);
        this.botsLastReport[bot.id] = new Date();

        bot.reportCallback = (b) => this._receiveBotReport(b);

        if (!this.timerObj)
            this.timerObj = setInterval(() => this._monitorBots(), 60 * 1000 * 10); //run every 10m
    }

    unmonitorBot(bot) {
        let botIndex = this.botsToMonitor.indexOf(bot);
        if (botIndex > -1)
            this.botsToMonitor.splice(botIndex, 1);

        if (this.timerObj && this.botsToMonitor.length == 0) {
            clearInterval(this.timerObj);
            this.timerObj = null;
        }
    }

    _receiveBotReport(bot) {
        this.botsLastReport[bot.id] = new Date();

        // Check if the bot was market as unresponsive previously
        let botIndex = this.unresponsiveBots.indexOf(bot);
        if (this.unresponsiveBots.indexOf(bot) != -1) {
            this.unresponsiveBots.splice(botIndex, 1);
            this.botsToMonitor.push(bot);

            var message = `Bot has come back ${emoji.get('ghost')}`;
            message = "Bot " + bot.id + " (" + bot.targetMarket + ")> " + message;

            log(message, true);
        }
    }

    _monitorBots() {
        for (let i in this.botsToMonitor) {
            let bot = this.botsToMonitor[i];

            let maximumDelayBetweenReports = new Date();
            maximumDelayBetweenReports.setTime(maximumDelayBetweenReports.getTime() - 1000 * 60 * minutesToTriggerAlert);

            if (maximumDelayBetweenReports > this.botsLastReport[bot.id]) {
                let botIndex = this.botsToMonitor.indexOf(bot);
                if (botIndex > -1)
                    this.botsToMonitor.splice(botIndex, 1);

                this.unresponsiveBots.push(bot);

                // Keep only 10 elements in this list
                if (this.unresponsiveBots.length > 10)
                    this.unresponsiveBots.shift();

                var message = `Bot hasn't reported in ${minutesToTriggerAlert} minute(s) ${emoji.get('warning')}:\n${JSON.stringify(bot)}`
                message = "Bot " + bot.id + " (" + bot.targetMarket + ")> " + message;

                log(message, true);
            }
        }
    }
}

module.exports = BotsMonitor;