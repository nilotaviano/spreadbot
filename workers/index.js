﻿var balance_reporter = require('./balance_reporter');
var net_profit_file_handler = require('./net_profit_file_handler')

function initialize(exchange) {
  balance_reporter(exchange);
  net_profit_file_handler.initialize();
}

module.exports = {
    initialize: initialize
};