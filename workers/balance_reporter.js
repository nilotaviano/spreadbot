﻿'use strict'

const TelegramBot = require('node-telegram-bot-api');
const emoji = require('node-emoji')
const helper = require("./../helper");
const fs = require('fs');

const token = process.env.TELEGRAM_BOT_TOKEN;
const balanceReportThreshold = process.env.TELEGRAM_BALANCE_CHANGE_THRESHHOLD || 0.1;
let bot = null;

const baseCurrency = process.env.BASE_MARKET;

let exchange = null;

let lastReportedBalance = null;
let chatId = null;
let timerObj = null;

async function initialize(e) {
  exchange = e;

  if (token) {
    bot = new TelegramBot(token, { polling: true });

    fs.readFile('telegramData.json', 'utf8', function (err, data) {
      if (!err) {
        var telegramData = JSON.parse(data);
        chatId = telegramData.chatId;

        // Send report 10 seconds after bot startup
        timerObj = setTimeout(reportBalance, 10 * 1000, true);
      }
    });

    bot.onText(/\/balance/, (msg, match) => {
      if (!chatId) {
        chatId = msg.chat.id;

        fs.writeFile('telegramData.json', JSON.stringify({ chatId: chatId }), function (err) {
          if (err)
            console.error("Error writing telegramData.json:\n" + JSON.stringify(err));
        });
      }
      reportBalance(true);
    });
  }
}

async function reportBalance(wasRequested) {
  calculateBalance()
    .then((balanceInBaseCurrency) => {
      let message = `${balanceInBaseCurrency.toFixed(8)} ${baseCurrency}`;

      if (wasRequested || Math.abs(balanceInBaseCurrency - lastReportedBalance) > balanceReportThreshold) {
        if (lastReportedBalance) {
          if (balanceInBaseCurrency > lastReportedBalance) {
            message += ' ' + emoji.get('rocket');
          }
          else if (balanceInBaseCurrency < lastReportedBalance) {
            message += ' ' + emoji.get('chart_with_downwards_trend');
          }
        }

        lastReportedBalance = balanceInBaseCurrency;
        bot.sendMessage(chatId, message)
          .catch(error => console.log(`Error trying to send balance report message: ${error}`));
      }

      if (timerObj)
        clearTimeout(timerObj);

      timerObj = setTimeout(reportBalance, 60 * 10 * 1000);
    })
    .catch((err) => {
      if (timerObj)
        clearTimeout(timerObj);

      timerObj = setTimeout(reportBalance, 60 * 10 * 1000);
    });
}

async function calculateBalance() {
  return new Promise((resolve, reject) => {
    exchange.getbalances((response) => {
      if (response.success) {
        let availableBalances = response.result;

        exchange.getmarketsummaries((response) => {
          if (response.success) {
            let marketSummaries = response.result;

            let availableBaseCurrencyBalance = availableBalances.find(r => r.Currency == baseCurrency);
            let balanceInBaseCurrency = availableBaseCurrencyBalance ? availableBaseCurrencyBalance.Balance : 0;

            availableBalances = availableBalances.filter(b => b.Balance > 0);

            for (let i in availableBalances) {
              let balance = availableBalances[i];
              let market = marketSummaries.find(ms => ms.MarketName == `${baseCurrency}-${balance.Currency}`);

              if (market) {
                let convertedBalance = balance.Balance * market.Bid;

                if (convertedBalance > exchange.getMinimumTrade())
                  balanceInBaseCurrency += convertedBalance;
              }
            }

            return resolve(balanceInBaseCurrency);
          }
          else {
            return reject(response.error);
          }
        });
      }
      else {
        return reject(response.error);
      }
    });
  });
}

module.exports = initialize;