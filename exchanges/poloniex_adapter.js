var poloniex = require('poloniex-api-node');

var apikey = process.env.APIKEY;
var apisecret = process.env.APISECRET;

var polo = new poloniex(apikey, apisecret, { socketTimeout: 2 * 60 * 1000 });

function adaptResponse(err, body) {
  var newResponse = {
    success: true,
    message: "",
    result: null
  };

  if (err) {
    newResponse.success = false;
    newResponse.message = err.message;
  }
  else
    newResponse.result = body;

  return newResponse;
}

function cancelorder(options, callback) {
  polo.cancelOrder(options.uuid, (err, body) => {
    var response = adaptResponse(err, body);

    if (response.message.indexOf("Invalid order number, or you are not the person who placed the order") !== -1) {
      response.message = "ORDER_NOT_OPEN";
    }

    callback(response);
  });
}

function adaptmarket(options) {
  if (options.market.indexOf("-") != -1)
    options.market = options.market.replace("-", "_");
}

function getorderbook(options, callback) {
  var parseOrders = (orderArray) => {
    var newOrderArray = [];

    if (orderArray)
      for (var i in orderArray)
        if (orderArray.hasOwnProperty(i)) {
          var order = orderArray[i];
          var newOrder = {
            "Rate": order[0],
            "Quantity": order[1]
          };
          newOrderArray.push(newOrder);
        }

    return newOrderArray;
  };

  adaptmarket(options);

  polo.returnOrderBook(options.market, options.depth, (err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.success && newResponse.result) {
      var newResult = {};

      newResult.buy = parseOrders(newResponse.result.bids);
      newResult.sell = parseOrders(newResponse.result.asks);

      newResponse.result = newResult;
    }

    callback(newResponse);
  });
}

function buylimit(options, callback) {
  adaptmarket(options);

  polo.buy(options.market, options.rate, options.quantity, false, false, false, (err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.success && newResponse.result)
      newResponse.result = {
        uuid: newResponse.result.orderNumber
      };

    callback(newResponse);
  });
}

function selllimit(options, callback) {
  adaptmarket(options);

  polo.sell(options.market, options.rate, options.quantity, false, false, false, (err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.message.indexOf("Total must be at least") !== -1 || newResponse.message.indexOf("Poloniex error 422") !== -1)
      newResponse.message = 'MIN_TRADE_REQUIREMENT_NOT_MET'

    if (newResponse.success && newResponse.result)
      newResponse.result = {
        uuid: newResponse.result.orderNumber
      };

    callback(newResponse);
  });
}

function getorder(options, callback) {
  var orderNumber = options.uuid;

  var findOrder = (allOpenOrders) => {
    if (allOpenOrders)
      for (var currencyPair in allOpenOrders)
        if (allOpenOrders.hasOwnProperty(currencyPair)) {
          var currencyPairOrders = allOpenOrders[currencyPair];

          for (var orderIndex in currencyPairOrders)
            if (currencyPairOrders.hasOwnProperty(orderIndex)) {
              if (currencyPairOrders[orderIndex].orderNumber == orderNumber)
                return currencyPairOrders[orderIndex];
            }
        }

    return null;
  };

  polo.returnOpenOrders('all', (err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.success) {
      var openOrder = findOrder(newResponse.result);

      polo.returnOrderTrades(orderNumber, (err, body) => {
        newResponse = adaptResponse(err, body);

        // If this error is returned, it means no trades were executed for the order
        if (newResponse.message.indexOf("Order not found, or you are not the person who placed it") !== -1) {
          newResponse.success = true;
          newResponse.result = [];
        }

        var isOrderOpen = openOrder != null;
        var total = 0;
        var fee = 0;
        var totalAmount = 0;
        var tradedAmount = 0;
        var orderType = '';

        // If succeeded, at least one trade was made in the order
        if (newResponse.success) {
          var trades = newResponse.result;

          for (var i in trades) {
            var trade = trades[i];

            total += parseFloat(trade.total);
            tradedAmount += parseFloat(trade.amount);
            fee = parseFloat(trade.fee); // The fee is returned in percentage, not the actual value
            orderType = trade.type;
          }
        }

        // On poloniex, the fee is taken from the amount purchased or the total of a sale, so we have to remove it from here
        // Rounded down to 8 decimal places
        if (orderType == 'buy')
          tradedAmount = Math.floor((tradedAmount * (1 - fee) * 1e8)) / 1e8;
        else if (orderType == 'sell')
          total = Math.floor((total * (1 - fee) * 1e8)) / 1e8;

        if (openOrder) {
          totalAmount = openOrder.amount;
          orderType = openOrder.type;
        }
        else
          totalAmount = tradedAmount;

        orderType = orderType == 'buy' ? 'LIMIT_BUY' : 'LIMIT_SELL';
        var unitPrice = tradedAmount != 0 ? total / tradedAmount : 0;
        var amountRemaining = totalAmount - tradedAmount;
        var cancelled = !isOrderOpen && (amountRemaining != totalAmount || totalAmount == 0);

        var order = {
          IsOpen: isOrderOpen,
          OrderUuid: orderNumber,
          Type: orderType,
          CancelInitiated: cancelled,
          Quantity: totalAmount,
          QuantityRemaining: amountRemaining,
          PricePerUnit: unitPrice,
          Price: total,
          CommissionPaid: 0 // TODO: Poloniex already includes the fee in the total trade cost, so I put 0 here to prevent the bot from adding/subtracting the fee from the total
        };

        newResponse.result = order;

        callback(newResponse);
      });
    }
    else
      callback(newResponse);
  });
}

function getbalances(callback) {
  polo.returnCompleteBalances('exchange', (err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.success) {
      var balances = [];
      for (var currency in newResponse.result) {
        var availableBalance = {
          Currency: currency,
          Available: parseFloat(newResponse.result[currency].available),
          Balance: parseFloat(newResponse.result[currency].available) + parseFloat(newResponse.result[currency].onOrders)
        };
        balances.push(availableBalance);
      }
      newResponse.result = balances;
    }

    callback(newResponse);
  });
}

function getmarkets(callback) {
  // The get markets service is being called only to get markets notice for now, like if a market is being delisted, 
  // poloniex doesn't seem to have such a service
  var response = {
    success: true,
    message: null,
    result: []
  };

  callback(response);
}

function getmarketsummaries(callback) {

  // TODO: Poloniex doesn't seem to have such a info
  var fakeCreatedDate = new Date();
  fakeCreatedDate.setMonth(fakeCreatedDate.getMonth() - 1);

  polo.returnTicker((err, body) => {
    var newResponse = adaptResponse(err, body);

    if (newResponse.success) {
      var newResult = [];

      for (var currencyPair in newResponse.result) {
        var marketName = currencyPair.replace("_", "-");
        var marketSummary = newResponse.result[currencyPair];

        var newMarketSummary = {
          Ask: marketSummary.lowestAsk,
          Bid: marketSummary.highestBid,
          BaseVolume: marketSummary.baseVolume,
          Created: fakeCreatedDate,
          MarketName: marketName,
          IsFrozen: marketSummary.isFrozen == 1
        };

        newResult.push(newMarketSummary);
      }

      newResponse.result = newResult;
    }

    callback(newResponse);
  });
}

function getMinimumTrade(baseMarket) {
  return 0.00050000;
}

module.exports = {
  getorderbook,
  cancelorder,
  buylimit,
  selllimit,
  getorder,
  getbalances,
  getmarkets,
  getmarketsummaries,
  defaultFee: 0.15 / 100, // 0.15%
  getMinimumTrade
};
