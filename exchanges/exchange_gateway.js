var bittrex_adapter = require('./bittrex_adapter');
var poloniex_adapter = require('./poloniex_adapter');

module.exports = {
  bittrex: bittrex_adapter,
  poloniex: poloniex_adapter
};