var request = require('request');
var exchangeGateway = require('./exchanges/exchange_gateway.js');
var Bot = require('./bot');
var logger = require('./logger');
const log = logger.log;
const error = logger.error;
var helper = require('./helper');
var BotsMonitor = require('./workers/bots_monitor.js');
var workers = require('./workers');
var recordActualProfitFromBot = require('./workers/net_profit_file_handler').recordActualProfitFromBot;

var undesiredMarketMessages = [
    "removed",
    "delisted",
    "deleted",
    "swapping"
];

var permanentlyBlacklistedMarkets = [
    'SPR', // Abandoned project
    'SBD', // SteemDollars - Pegged to USD
    'TUSD',  // Pegged to USD
    'NBT',  // Pegged to USD
    'LBC', // RATE_PRECISION_NOT_ALLOWED - temp fix
];

// Shitcoins that have the same ticker as coins on our exchanges
var ignoredCoinMarketCapcoins = [
    'batcoin',
    'bitconnect',
    'rcoin'
];

var botsMonitor = new BotsMonitor();

var stopMessageReceived = false;

const runParameters = {};
runParameters.botActionTimeoutInMS = process.env.TIMEOUT_MS || 100;
runParameters.maximumActiveBots = process.env.ACTIVE_BOTS || 2;
runParameters.targetMarket = null; // The specific coin to look for
runParameters.baseMarketToSeek = process.env.BASE_MARKET; // The base market: ETH, BTC, USDT
runParameters.shouldAccumulate = !!process.env.SHOULD_ACCUMULATE; // default false
runParameters.shouldBlacklist = process.env.SHOULD_BLACKLIST != null ? process.env.SHOULD_BLACKLIST : true;
runParameters.blacklistRemovalTimeoutInMinutes = process.env.BLACKLIST_REMOVAL_MINUTES || 120;
runParameters.checkCoinMarketPrices = process.env.CHECK_COINMARKETCAP_PRICES != null ? process.env.CHECK_COINMARKETCAP_PRICES : true;
runParameters.maximumPriceIncreaseFromCMC = process.env.MAXIMUM_PRICE_INCREASE_FROM_CMC || 1.03;
runParameters.hoursBeforeRetryingToGetProfit = process.env.HOURS_BEFORE_RETRYING_TO_GET_PROFIT;
runParameters.maximumPriceIncreaseRatioFromPreviousDay = process.env.MAXIMUM_PRICE_INCREASE_RATIO_FROM_PREVIOUS_DAY;
runParameters.additionalUndesiredMessages = process.env.UNDESIRED_MESSAGES;

var exchangeName = process.env.EXCHANGE || 'bittrex';
var exchange = exchangeGateway[exchangeName];

if (exchange == null) throw new Error("Invalid exchange");

workers.initialize(exchange);

var allMarkets;

var config = null;
if (process.env.CONFIG) {
  config = JSON.parse(process.env.CONFIG);
}
else {
  config = {
    'BTC': {
      minimumSpread: 1.01, // The quocient of ask/bid
      minimumVolume: 100, // The minimum volume to be considered in targetBaseCurrency of the market
      minimumAmountToInvest: 0.007, // The minimum amount of targetBaseCurrency to invest
      maximumNegotiatedAmount: 0.007,
      minimumPrice: 0.00001000
    },
    'ETH': {
      minimumSpread: 1.05, // The quocient of ask/bid
      minimumVolume: 350, // The minimum volume to be considered in targetBaseCurrency of the market
      minimumAmountToInvest: 0.08, // The minimum amount of targetBaseCurrency to invest
      maximumNegotiatedAmount: 0.08,
      minimumPrice: 0.00020000
    },
    'XMR': {
      minimumSpread: 1.05, // The quocient of ask/bid
      minimumVolume: 70, // The minimum volume to be considered in targetBaseCurrency of the market
      minimumAmountToInvest: 0.08, // The minimum amount of targetBaseCurrency to invest
      maximumNegotiatedAmount: 0.08,
      minimumPrice: 0.00003500
    },
    'USDT': {
      minimumSpread: 1.05, // The quocient of ask/bid
      minimumVolume: 50000, // The minimum volume to be considered in targetBaseCurrency of the market
      minimumAmountToInvest: 0.08, // The minimum amount of targetBaseCurrency to invest
      maximumNegotiatedAmount: 0.08,
      minimumPrice: 0.015
    }
  };
}

function onRunParametersChanged(options) {
    process.send({
        type: 'process:msg',
        data: {
            topic: 'change_params',
            success: true,
            options: options
        }
    });
}

function onBotStop() {
    log('Bot stopped', true);
    process.send({
        type: 'process:msg',
        data: {
            topic: 'stop',
            success: true
        }
    });
}

var activeBots = [];
var botIndex = 0;
var allocatedMarkets = {};
var availableBalances;

var subsequentLossesCountByMarket = {};
var blackListedMarketsTimestamp = {};

var lossesInSequence = 0;
var netProfit = 0;

updateDependentParameters();

start();
scheduleBlacklistCheck();

function updateDependentParameters() {
    if (runParameters.additionalUndesiredMessages)
    {
        var additionalMessagesArray = JSON.parse(runParameters.additionalUndesiredMessages);

        if (additionalMessagesArray.hasOwnProperty("length"))
        {
            undesiredMarketMessages = undesiredMarketMessages.concat(additionalMessagesArray);
        }
    }
}

function start() {
    if (stopMessageReceived) {
        if (activeBots.length == 0)
            onBotStop();

        return;
    }
    
  updateAvailableBalances();
}

function updateAvailableBalances() {
  exchange.getbalances((response) => {
    if (response.success) {
      availableBalances = response.result;
      validateConfigsAndRun();
    }
    else {
      log(response.message);
      setTimeout(start, 10000);
    }
  });
}

function validateConfigsAndRun() {
  if (mustStop())
    return;

  if (activeBots.length >= runParameters.maximumActiveBots) {
    setTimeout(validateConfigsAndRun, 2000);
  }
  else if (runParameters.baseMarketToSeek != null && !config.hasOwnProperty(runParameters.baseMarketToSeek)) {
    log("Configuration not found for desired market " + runParameters.baseMarketToSeek);
  }
  else if (!hasAnyMarketWithAvailableBalance()) {
    log("No market found with minimum available balance to invest");
    setTimeout(updateAvailableBalances, 1000);
  }
  else if (activeBots.length >= runParameters.maximumActiveBots)
    log("Not enough bots to keep processing");
  else
    tryToInvestInSomething();
}

function hasAnyMarketWithAvailableBalance() {
  var anyBalanceAvailable = false;
  if (runParameters.baseMarketToSeek != null) {
    if (isCurrencyBalanceAvailable(runParameters.baseMarketToSeek))
      anyBalanceAvailable = true;
  }
  else if (runParameters.targetMarket != null) {
    var baseCurrency = getBaseCurrencyFromMarketName(runParameters.targetMarket);
    if (isCurrencyBalanceAvailable(baseCurrency))
      anyBalanceAvailable = true;
  }
  else {
    for (var baseCurrency in config)
      if (isCurrencyBalanceAvailable(baseCurrency)) {
        anyBalanceAvailable = true;
        break;
      }
  }

  return anyBalanceAvailable;
}

function isCurrencyBalanceAvailable(currencyName) {
  var availableBalanceEntry = getAvailableBalanceEntryForCurrency(currencyName);

  return availableBalanceEntry && availableBalanceEntry.Available > 0 && (!config[currencyName].minimumAmountToInvest || availableBalanceEntry.Available > config[currencyName].minimumAmountToInvest);
}

function tryToInvestInSomething() {
  if (runParameters.targetMarket != null)
    runBotForMarket(runParameters.targetMarket);
  else if (lossesInSequence >= 3 && runParameters.hoursBeforeRetryingToGetProfit)
    waitBeforeRetryingToGetProfit();
  else if (runParameters.checkCoinMarketPrices && runParameters.baseMarketToSeek)
    getCoinMarketCapTickers(lookForTargetMarket);
  else
    lookForTargetMarket();
}

function waitBeforeRetryingToGetProfit() {
  lossesInSequence = 0;
  log(`Waiting ${runParameters.hoursBeforeRetryingToGetProfit} hours before retrying to get profit.`, true);
  setTimeout(tryToInvestInSomething, runParameters.hoursBeforeRetryingToGetProfit * 60 * 60 * 1000)
}

function lookForTargetMarket(coinmarketcapTickers) {
  if (mustStop())
    return;

  getMarkets((markets) => {

    allMarkets = markets.reduce((result, item, index, array) => {
      result[item.MarketName] = item;
      return result;
    }, {});

    undesirableMarkets = filterUndesirableMarkets(markets);

    exchange.getmarketsummaries(function(response) {
      if (!response.success || !helper.notEmptyArray(response.result)) {
        log('Error trying to getmarketsummaries: ' + response.error);
        setTimeout(lookForTargetMarket, 2000);
      }
      else if (!mustStop()) {

        var biggestSpread = {
          spread: 0,
          market: null,
          volume: 0,
          baseCurrency: null
        };

        for (var i in response.result) {
          var marketName = response.result[i].MarketName;
          var baseCurrency = getBaseCurrencyFromMarketName(marketName);

          var bid = response.result[i].Bid;
          var ask = response.result[i].Ask;

          var availableBalanceForMarket = getAvailableBalanceEntryForMarket(marketName);

          var prevDay = response.result[i].PrevDay;
          var priceIncreaseRatio = bid / prevDay;

          if (runParameters.baseMarketToSeek != null && baseCurrency != runParameters.baseMarketToSeek)
            continue;
          else if (!config.hasOwnProperty(baseCurrency))
            continue;
          else if (config[baseCurrency].minimumPrice != null && bid < config[baseCurrency].minimumPrice)
            continue;
          else if (allocatedMarkets.hasOwnProperty(marketName)) // If the market is already allocated, ignore
            continue;
          else if (blackListedMarketsTimestamp.hasOwnProperty(marketName))
            continue;
          else if (!availableBalanceForMarket || availableBalanceForMarket <= 0)
            continue;
          else if (isNewMarket(response.result[i]))
            continue;
          else if (undesirableMarkets.indexOf(response.result[i].MarketName) >= 0)
            continue;
          else if (coinmarketcapTickers && isExchangePriceHigherThanCMC(coinmarketcapTickers, response.result[i]))
            continue;
          else if (runParameters.maximumPriceIncreaseRatioFromPreviousDay && priceIncreaseRatio > runParameters.maximumPriceIncreaseRatioFromPreviousDay)
            continue;
          else if (response.result[i].IsFrozen) // Only on poloniex
            continue;

          var spread = ask / bid;

          if (spread > biggestSpread.spread && response.result[i].BaseVolume > config[baseCurrency].minimumVolume) {
            biggestSpread.spread = spread;
            biggestSpread.market = response.result[i].MarketName;
            biggestSpread.volume = response.result[i].BaseVolume;
            biggestSpread.baseCurrency = baseCurrency;
            biggestSpread.bid = bid;
          }
        }

        if (config[biggestSpread.baseCurrency] && biggestSpread.spread >= config[biggestSpread.baseCurrency].minimumSpread) {
          var message = 'Target market found: ' + biggestSpread.market + '. Spread: ' + biggestSpread.spread;
          log(message);

          runBotForMarket(biggestSpread.market, biggestSpread.bid);
        }
        else {
          log('Market with biggest spread is not satisfactory: ' + JSON.stringify(biggestSpread));
          setTimeout(lookForTargetMarket, 1000);
        }
      }
    });
  });
}

function isExchangePriceHigherThanCMC(coinmarketcapTickers, market) {
  var coinmarketcapTicker = coinmarketcapTickers[market.MarketName];

  return coinmarketcapTicker && market.Bid > coinmarketcapTicker.price * runParameters.maximumPriceIncreaseFromCMC; // Maximum 3% of difference
}

function getCoinMarketCapTickers(callback, tries = 1) {
  request(`https://api.coinmarketcap.com/v1/ticker/?limit=0&convert=${runParameters.baseMarketToSeek}`, function (error, response, body) {
    if (!error && response.statusCode == 200 && IsJson(response.body)) {
      var responseJson = JSON.parse(response.body);
      var markets = {};

      for (var i in responseJson) {
        if (!markets.hasOwnProperty(responseJson[i].symbol) && ignoredCoinMarketCapcoins.indexOf(responseJson[i].id) == -1) {
          var marketName = `${runParameters.baseMarketToSeek}-${responseJson[i].symbol}`; // BTC-ETH, ETH-1ST, etc.

          responseJson[i]['price'] = responseJson[i][`price_${runParameters.baseMarketToSeek.toLowerCase()}`];
          markets[marketName] = responseJson[i];
        }
      }

      callback(markets);
    }
    else {
      if (error) {
        console.error(`Request error on coinmarketcap: ${error}`)
      }

      if (tries <= 3)
        setTimeout(() => getCoinMarketCapTickers(callback, tries + 1), 100);
      else
        callback(null);
    }
  })
}

function IsJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function getMarkets(callback) {
  exchange.getmarkets((response) => {
    if (!response.success) {
      log('Error trying to getmarkets: ' + response.error);
      setTimeout(lookForTargetMarket, 2000);
    }
    else {
      callback(response.result);
    }
  });
}

function filterUndesirableMarkets(markets) {
  return markets.filter(isUndesirable).map(m => m.MarketName);
}

function isUndesirable(market) {
    if (!market.IsActive || permanentlyBlacklistedMarkets.indexOf(market.MarketCurrency) > -1)
        return true;

    if (!market.Notice)
        return false;

    var marketNotice = market.Notice.toLowerCase();

    for (var i in undesiredMarketMessages)
    {
        var undesiredMarketMessage = undesiredMarketMessages[i];

        if (marketNotice.includes(undesiredMarketMessage))
            return true;
    }

    return false;
}

function isNewMarket(marketSummary) {
  var marketCreationDate = new Date(Date.parse(marketSummary.Created));
  var hoursSinceCreation = Math.abs(new Date() - marketCreationDate) / 36e5;

  return hoursSinceCreation < 24;
}

function runBotForMarket(targetMarket, startingBidPrice) {
  if (activeBots.length >= runParameters.maximumActiveBots) {
    var message = "Not enough bots to process market: " + targetMarket;
    log(message);
    return;
  }

  if (mustStop())
    return;

  var baseCurrency = getBaseCurrencyFromMarketName(targetMarket);
  var amountToAllocate = getAmountToAllocate(baseCurrency);
  var minTradeSize = allMarkets[targetMarket].MinTradeSize;

  allocatedMarkets[targetMarket] = 1;
  botIndex += 1;

  var bot = new Bot(botIndex, targetMarket, amountToAllocate, config[baseCurrency].minimumSpread, deallocateBot, runParameters.botActionTimeoutInMS, exchange, runParameters.shouldAccumulate, startingBidPrice, minTradeSize);
  activeBots.push(bot);
  botsMonitor.monitorBot(bot);
  bot.start();

  validateConfigsAndRun();
}

function getAmountToAllocate(baseCurrency) {
  var availableBalanceEntry = getAvailableBalanceEntryForCurrency(baseCurrency);

  var amountToAllocate = 0;

  if (availableBalanceEntry) {
    amountToAllocate = Math.min(config[baseCurrency].maximumNegotiatedAmount, availableBalanceEntry.Available);
    amountToAllocate = discountTxFee(amountToAllocate);

    availableBalanceEntry.Available -= amountToAllocate;
  }

  return amountToAllocate;
}

function discountTxFee(amount) {
  return amount * 0.9975;
}

function getAvailableBalanceEntryForMarket(marketName) {
  var baseCurrency = getBaseCurrencyFromMarketName(marketName);
  return getAvailableBalanceEntryForCurrency(baseCurrency);
}

function getAvailableBalanceEntryForCurrency(baseCurrency) {
  return availableBalances.find(r => r.Currency == baseCurrency);
}

function getBaseCurrencyFromMarketName(marketName) {
  return marketName.split('-')[0];
}

function deallocateBot(botToDeallocate) {
  if (!botToDeallocate) {
    log("Error trying to deallocate bot. Undefined bot");
    return;
  }

  var botIndex = activeBots.indexOf(botToDeallocate);
  if (botIndex > -1)
    activeBots.splice(botIndex, 1);

  botsMonitor.unmonitorBot(botToDeallocate);

  recordActualProfitFromBot(botToDeallocate);

  var percentageProfit = ((botToDeallocate.balance + botToDeallocate.accumulatedEquivalentInBaseCurrency) / botToDeallocate.initialBalance) * 100 - 100;

  sendMessageForBotDeallocation(botToDeallocate, percentageProfit);

  verifyMarketPerformance(botToDeallocate.targetMarket, percentageProfit);

  if (percentageProfit < 0)
    lossesInSequence++;
  else if (percentageProfit > 0)
    lossesInSequence = 0;

  deallocateBalance(botToDeallocate.targetMarket, botToDeallocate.balance);
  deallocateMarket(botToDeallocate.targetMarket);
}

function sendMessageForBotDeallocation(deallocatedBot, profit) {
  var profitMessage = "";

  if (profit != 0) {
    profitMessage = " with " + (profit > 0 ? "profit" : "deficit");
    profitMessage = profitMessage + " of " + profit + "%";
  }

  var message = "Bot " + deallocatedBot.id + " deallocated from " + deallocatedBot.targetMarket + profitMessage;
  log(message);
}

function deallocateBalance(targetMarket, balanceToDeallocate) {
  if (balanceToDeallocate) {
    var currencyBalanceEntry = getAvailableBalanceEntryForMarket(targetMarket);
    currencyBalanceEntry.Available += balanceToDeallocate;
  }
}

function deallocateMarket(marketToDeallocate) {
  if (marketToDeallocate && allocatedMarkets.hasOwnProperty(marketToDeallocate))
    delete allocatedMarkets[marketToDeallocate];
}

function verifyMarketPerformance(market, profit) {
  if (!runParameters.shouldBlacklist)
    return;

  if (subsequentLossesCountByMarket.hasOwnProperty(market)) {
    if (profit > 0)
      subsequentLossesCountByMarket[market]--;
    else if (profit < 0)
      subsequentLossesCountByMarket[market]++;
  }
  else if (profit < 0) {
    subsequentLossesCountByMarket[market] = 1;
  }

  if (subsequentLossesCountByMarket[market] == 0) {
    delete subsequentLossesCountByMarket[market];
  }
  else if (subsequentLossesCountByMarket[market] > 2) {
    addMarketToBlacklist(market);
  }
}

function scheduleBlacklistCheck() {
  if (!runParameters.shouldBlacklist)
    return;

  // This is done in a setTimeout way to allow the interval to be changed dinamically by change_params
  setTimeout(checkBlacklistedMarkets, runParameters.blacklistRemovalTimeoutInMinutes * 60 * 1000);
}

function checkBlacklistedMarkets() {
  for (var market in blackListedMarketsTimestamp)
    tryRemoveMarketFromBlacklist(market);

  scheduleBlacklistCheck();
}

function addMarketToBlacklist(market) {
  blackListedMarketsTimestamp[market] = new Date();

  log(`${market} was blacklisted`, true);
}

function tryRemoveMarketFromBlacklist(market) {
  if (!blackListedMarketsTimestamp.hasOwnProperty(market))
    return;

  var blacklistTimestamp = blackListedMarketsTimestamp[market];
  var now = new Date();

  if (now.getTime() - blacklistTimestamp.getTime() > runParameters.blacklistRemovalTimeoutInMinutes * 60 * 1000) {
    delete blackListedMarketsTimestamp[market];

    subsequentLossesCountByMarket[market] = 0;

    log(`${market} was removed from blacklist`, true);
  }
}

function mustStop() {
    var stop = false;

    // All stop conditions must be implemented here
    if (stopMessageReceived) {
        stop = true;
        start();
    }

    return stop;
}

function stop() {
    log('Stop message received', true);
    stopMessageReceived = true;

    for (var i in activeBots) {
        if (activeBots.hasOwnProperty(i)) {
            activeBots[i].stop();
        }
    }
}

function changeParams(options) {
    log('Change parameters message received: ' + JSON.stringify(options), true);

    var baseMarket = options.baseMarket;

    var objToChange;

    if (baseMarket)
        objToChange = config[baseMarket];
    else
        objToChange = runParameters;

    if (objToChange && objToChange.hasOwnProperty(options.argumentName))
        objToChange[options.argumentName] = options.argumentValue;

    updateDependentParameters();

    onRunParametersChanged(options);
}

process.on('message', function (packet) {
    if (packet.topic == 'stop')
        stop();
    else if (packet.topic == 'params') {
        if (packet.type == 'change')
            changeParams(packet.data);
    }
});
